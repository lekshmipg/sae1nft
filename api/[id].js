const MetaDataSet = require("../Metadata");

module.exports = async (req, res) => {
    const { id } = await req.query;
    // console.log(MetaDataSet[id]);
    await res.json(JSON.parse(JSON.stringify(MetaDataSet[id])))
}

